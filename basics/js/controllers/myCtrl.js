app.controller('myCtrl', ['$scope', function($scope) { 
    $scope.title = 'This Month\'s Bestsellers'; 
    $scope.promo = 'The most popular books this month.';
    $scope.products = [ 
      { 
        name: 'The Book of Trees', 
        price: 19, 
        pubdate: new Date('2014', '03', '08'), 
        cover: 'img/the-book-of-trees.jpg',
        likes: 0,
        dislikes: 0
      }, 
      { 
        name: 'Books Cartoon', 
        price: 8, 
        pubdate: new Date('2013', '08', '01'), 
        cover: 'img/books-cartoon.png',
        likes: 0,
        dislikes: 0
      },
      { 
        name: 'Java Network Programming', 
        price: 12, 
        pubdate: new Date('2017', '07', '02'), 
        cover: 'img/Java-network-programming.jpg', likes: 0 ,
        dislikes: 0
      },
      { 
        name: 'Java Threads', 
        price: 11, 
        pubdate: new Date('2019', '04', '12'), 
        cover: 'img/Java-Threads.jpg',
        likes: 0,
        dislikes: 0 
      }
    ];
    $scope.plusOne=function(index) { 
      $scope.products[index].likes += 1; 
    };
    
    $scope.minusOne=function(index) { 
      $scope.products[index].dislikes += 1; 
    };
  }]);

  